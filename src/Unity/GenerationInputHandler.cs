﻿using System;
using UnityEngine;

//This class handles dealing with input from the Unity UI system
public class GenerationInputHandler : MonoBehaviour
{
    public GameObject generalMenu;
    public GameObject PerlinDefaultMenu;
    public GameObject PerlinExtraMenu;
    public GameObject VolcanoMenu;

    //General Settings
    private GenerationType type = GenerationType.Random;
    private int size = 5;
    private int minHeight = 0;
    private int maxHeight = 15;

    //Volcano
    private int amount = 10;
    private int radius = 10;
    private int distance = -1;

    //Perlin Default 
    private float delta = 0.7f;

    //Perlin Extra 
    private int countBeforeChange = 100;

    public UnityWorldGenerator generator;

    void Start()
    {
        generalMenu.SetActive(true);
        PerlinDefaultMenu.SetActive(false);
        PerlinExtraMenu.SetActive(false);
        VolcanoMenu.SetActive(false);
    }

    //acts as setters (need to be in method form because of how unity implements UI)
    public void setType(int typeNum)
    {
        switch (typeNum)
        {
            case 0:
                type = GenerationType.Random;
                generalMenu.SetActive(true);
                PerlinDefaultMenu.SetActive(false);
                PerlinExtraMenu.SetActive(false);
                VolcanoMenu.SetActive(false);
                break;
            case 1:
                type = GenerationType.Perlin;
                generalMenu.SetActive(true);
                PerlinDefaultMenu.SetActive(true);
                PerlinExtraMenu.SetActive(true);
                VolcanoMenu.SetActive(false);
                break;
            case 2:
                type = GenerationType.Volcano;
                generalMenu.SetActive(true);
                PerlinDefaultMenu.SetActive(false);
                PerlinExtraMenu.SetActive(false);
                VolcanoMenu.SetActive(true);
                break;
            case 3:
                type = GenerationType.PerlinSquare;
                generalMenu.SetActive(true);
                PerlinDefaultMenu.SetActive(true);
                PerlinExtraMenu.SetActive(false);
                VolcanoMenu.SetActive(false);
                break;
        }
    }

    public void setSize(string sizeString)
    {
        parseIntIgnoreError(sizeString, ref size);
    }

    public void setMinHeight(string minHeightString)
    {
        parseIntIgnoreError(minHeightString, ref minHeight);
    }

    public void setMaxHeight(string maxHeightString)
    {
        parseIntIgnoreError(maxHeightString, ref maxHeight);
    }

    public void setAmount(string amountString)
    {
        parseIntIgnoreError(amountString, ref amount);
    }

    public void setRadius(string radiusString)
    {
        parseIntIgnoreError(radiusString, ref radius);
    }

    public void setDistance(string distanceString)
    {
        parseIntIgnoreError(distanceString, ref distance);
    }

    public void setDelta(string deltaString)
    {
        parseFloatIgnoreError(deltaString, ref delta);
    }

    public void setCountBeforeChange(string countBeforeChangeString)
    {
        parseIntIgnoreError(countBeforeChangeString, ref countBeforeChange);
    }

    //Using reference to avoid using if statement when exception is caught (oe checking for if return value < -1)
    private void parseIntIgnoreError(string s, ref int i)
    {
        try { i = int.Parse(s); }
        catch (Exception) { }
    }

    private void parseFloatIgnoreError(string s, ref float i)
    {
        try { i = float.Parse(s); }
        catch (Exception) { }
    }

    //When generate button is pressed this method handles creating the appropriate world
    public void generate()
    {
        switch (type)
        {
            case GenerationType.Random:
                generator.setupRandomWorld(size, minHeight, maxHeight);
                break;
            case GenerationType.Perlin:
                generator.setupPerlinWorld(size, minHeight, maxHeight, delta, countBeforeChange);
                break;
            case GenerationType.Volcano:
                generator.setupVolcanoWorld(size, minHeight, maxHeight, amount, radius, distance);
                break;
            case GenerationType.PerlinSquare:
                generator.setupPerlinSquareWorld(size, minHeight, maxHeight, delta);
                break;
        }
        generator.generateWorld();
    }
}
