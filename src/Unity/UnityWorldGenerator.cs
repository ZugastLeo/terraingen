﻿using UnityEngine;

//Acts as Wrapper class for the different types of generators so they work in unity
public class UnityWorldGenerator : MonoBehaviour
{
    //Unity GameObject to represent the grids in the map
    public GameObject plane;
    //vertex amount per side of plane
    private int vertexSideCount = 11;

    //List of all generated planes used on the map
    private GameObject[,] planes;
    //Number of planes used on the map
    private int size = 1 ;

    //WorldGenerator object used to create a heightMap
    private WorldGenerator generator;
    //checks if world has been set doesn't allow display otherwise
    private bool isWorldSet = false;

    //sets up generator (similar principle to constructors) 
    //the problem with constructors is that it wouldn't be clear which type of generator it will use
    //and the input is different depending on which it will use.
    //this also allows to setup different type of generation without 
    public void setupRandomWorld(int size, int minHeight, int maxHeight)
    {
        generalSetup(size);
        this.generator = new RandomWorldGenerator((this.vertexSideCount - 1) * size + 1, minHeight, maxHeight);
    }

    public void setupPerlinWorld(int size, int minHeight, int maxHeight, float delta, int countBeforeChange)
    {
        generalSetup(size);
        this.generator = new PerlinWorldGenerator((this.vertexSideCount - 1) * size + 1, minHeight, maxHeight, delta, countBeforeChange);
    }

    public void setupPerlinSquareWorld(int size, int minHeight, int maxHeight, float delta)
    {
        generalSetup(size);
        this.generator = new PerlinSquareWorldGenerator((this.vertexSideCount - 1) * size + 1, minHeight, maxHeight, delta);
    }

    public void setupVolcanoWorld(int size, int minHeight, int maxHeight, int numVolcanos, int radius, int volcanoDistance)
    {
        generalSetup(size);
        this.generator = new VolcanoWorldGenerator((this.vertexSideCount - 1) * size + 1, minHeight, maxHeight, numVolcanos, radius, volcanoDistance);
    }

    //Generates World in Unity 
    public void generateWorld()
    {
        //generator object builds height map to be displayed
        generator.generateHeightMap();

        //loop though the square we want to make
        for (int planeX = 0; planeX < size; planeX++)
        {
            for (int planeZ = 0; planeZ < size; planeZ++)
            {
                //get necessary mesh and vector arrays
                GameObject currentPlane = GameObject.Instantiate(plane, new Vector3(planeX * (vertexSideCount - 1), 0, planeZ * (vertexSideCount - 1)), Quaternion.identity);
                Mesh mesh = currentPlane.GetComponent<MeshFilter>().mesh;
                Mesh meshCollider = currentPlane.GetComponent<MeshFilter>().mesh;
                Vector3[] vertices = mesh.vertices;


                for (int x = 0; x < vertexSideCount; x++)
                {
                    for (int z = 0; z < vertexSideCount; z++)
                    {
                        float height = Mathf.Max(0, generator.getHeight(x + planeX * (vertexSideCount - 1), z + planeZ * (vertexSideCount - 1)));
                        int index = (x % vertexSideCount) + (z % vertexSideCount) * vertexSideCount;
                        vertices[index] = new Vector3(x, height, z);
                    }
                }

                //set the necessary values
                mesh.SetVertices(vertices);
                mesh.vertices = vertices;
                meshCollider.SetVertices(vertices);
                meshCollider.vertices = vertices;
                currentPlane.GetComponent<MeshCollider>().sharedMesh = meshCollider;

                //recalculate vectors for graphics engine
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();

                meshCollider.RecalculateBounds();
                meshCollider.RecalculateNormals();
                meshCollider.RecalculateTangents();

                //set plane in proper plane array
                planes[planeX, planeZ] = currentPlane;
            }
        }
        isWorldSet = true;
    }

    //Destroys previous planes, allows for new world to be generated
    private void cleanUpPlanes()
    {
        //Check if world has been set
        if (!isWorldSet)
        {
            return;
        }

        Debug.Log("Cleaning...");

        for (int planeX = 0; planeX < size; planeX++)
        {
            for (int planeZ = 0; planeZ < size; planeZ++)
            {
                GameObject.Destroy(planes[planeX, planeZ]);
            }
        }
    }

    //regardless of type of world, it is useful for all
    private void generalSetup(int size)
    {
        cleanUpPlanes();
        this.size = size;
        this.planes = new GameObject[size, size];
    }
}


