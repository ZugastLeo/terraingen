﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Enum to represent Generation Types
public enum GenerationType 
{
    Random, Perlin, PerlinSquare, Volcano
}
