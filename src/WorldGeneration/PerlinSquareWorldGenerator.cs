﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Identical idea to normal PerlinSquareGenerator but:
 * The normal one generates in diagonals because of the way it cycles throught the coordinates
 * This implementaion tries to fix that by going around in squares.
 */
public class PerlinSquareWorldGenerator : WorldGenerator
{
    private float delta;

    /*
     * PerlinSquareWorldGenerator Constructor
     * 
     * size: determines a the side length of the terrain
     * minHeight: determines the min height of the generation
     * maxHeight: determines the max height of the generation
     * delta: determines the range of values the next step can take according to the previous value
     */
    public PerlinSquareWorldGenerator(int size, int minHeight, int maxHeight, float delta) : base(size, minHeight, maxHeight)
    {
        this.delta = delta;
    }

    public override void generateHeightMap()
    {
        int middle = size / 2;
        int x = 0;
        int z = 0;
        //Starts on the exterior of the terrain and works it's way inwards
        for (int radius = middle; radius >= 0; radius--)
        {
            //Each for loop handles 1 side of the square (4 loops = 1 square)
            z = middle - radius;
            for (x = middle - radius; x <= middle + radius; x++)
            {
                map[x, z] = calculateHeight(x, z);

            }
            x = middle + radius;
            for (z = middle - radius; z <= middle + radius; z++)
            {
                map[x, z] = calculateHeight(x, z);
            }

            z = middle + radius;
            for (x = middle + radius; x >= middle - radius; x--)
            {
                map[x, z] = calculateHeight(x, z);
            }

            x = middle - radius;
            for (z = middle + radius; z >= middle - radius; z--)
            {
                map[x, z] = calculateHeight(x, z);
            }
        }
    }

    /*
     * checks surrounding coordinates, if they have been set it uses them to calculate an average
     */
    private float calculateHeight(int x, int z)
    {
        int numValues = 0;
        float average = 0;
        //Each if statements checks a different surrounding coordinate
        if (x - 1 >= 0 && map[x - 1, z] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x - 1, z];
        }
        if (x - 1 >= 0 && z - 1 >= 0 && map[x - 1, z - 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x - 1, z - 1];
        }
        if (x - 1 >= 0 && z + 1 < size && map[x - 1, z + 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x - 1, z + 1];
        }
        if (x + 1 < size && map[x + 1, z] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x + 1, z];
        }
        if (x + 1 < size && z - 1 >= 0 && map[x + 1, z - 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x + 1, z - 1];
        }
        if (x + 1 < size && z + 1 < size && map[x + 1, z + 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x + 1, z + 1];
        }
        if (z + 1 < size && map[x, z + 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x, z + 1];
        }
        if (z - 1 >= 0 && map[x, z - 1] != WorldGenerator.NOTSET)
        {
            numValues++;
            average += map[x, z - 1];
        }

        //if there are surounding values that have been set it calculates height accordingly
        float height = 0;
        if (numValues > 0)
        {
            average = average / numValues;
            float offset = Random.Range(-delta, delta);
            height = Mathf.Clamp(average + offset, minHeight, maxHeight);
        }
        //otherwise it generates the height at random
        else
        {
            height = Random.Range((float)minHeight, maxHeight);
        }
        return height;
    }
}
