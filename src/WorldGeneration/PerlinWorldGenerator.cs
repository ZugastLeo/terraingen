﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * PerlinWorldGenerator:
 * 
 * The idea is to generate the first value randomly, and then choose an adjacent value within a range determined by the delta value accodring to the first value
 * Example: say we choose point (0,0) to have height 3, then (1, 0) can only be in [2.5, 3.5] (if delta is 0.5)
 * since this is a 2D surface it takes into consideration the one value at (x-1, z) and (x, z-1)
 * Furthermore, the countBeforeChange makes it so that for that amount of steps, it will favor either going up or going down, (example, if going up: value will be between [2.75, 3.5])
 * when the count runs out, the it will choose randomly to favor up or down.
 * This makes a "realistic" terrain that is random but still has smoothness.
 */
public class PerlinWorldGenerator : WorldGenerator
{
    private float delta;
    private int countBeforeChange;

    /*
     * PerlinWorldGenerator Constructor
     * 
     * size: determines a the side length of the terrain
     * minHeight: determines the min height of the generation
     * maxHeight: determines the max height of the generation
     * delta: determines the range of values the next step can take according to the previous value
     * countBeforeChange: how many steps before changing whether to favor going up or going down
     */
    public PerlinWorldGenerator(int size, int minHeight, int maxHeight, float delta, int countBeforeChange) : base(size, minHeight, maxHeight)
    {
        this.delta = delta;
        this.countBeforeChange = countBeforeChange;
    }

    /*
     * Generates height according the perlin generation
     */
    public override void generateHeightMap()
    {
        bool goingUp = false;
        int numSteps = 0;
        int x = 0;
        int z = 0;


        //This allows it to generate more "water" (its more likely to start at a lower value that if it was Random.Range(minHeight, maxHeight)
        map[x, z] = Mathf.Max(minHeight, Random.Range(-maxHeight, maxHeight));
        z++;

        //go through the whole map
        while (x < size)
        {
            while (z < size)
            {
                //Gets the average of surrounding values
                float average;
                if (z - 1 < 0)
                {
                    average = map[x - 1, z];
                }
                else if (x - 1 < 0)
                {
                    average = map[x, z - 1];
                }
                else
                {
                    average = (map[x, z - 1] + map[x - 1, z]) / 2;
                }

                //gets the desired offset
                float offset = Random.Range(-delta + (goingUp ? delta / 2: 0), delta + (!goingUp ? -delta / 2: 0));

                //applies the offset to average as height but doesnt go out of the limits determined by minHeight, maxHeight
                map[x, z] = Mathf.Clamp(average + offset, minHeight, maxHeight);
                z++;

                //If numSteps is the countBeforeChange then it sets a new prefered direction
                numSteps++;
                if (numSteps == countBeforeChange)
                {
                    numSteps = 0;
                    goingUp = Random.Range(0, 2) == 1;
                }
            }
            z = 0;
            x++;
        }
    }
}
