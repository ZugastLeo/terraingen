﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * RandomWorldGenerator:
 * 
 * Randomly sets each point on the map
 * This is mostly used for testing
 */
public class RandomWorldGenerator : WorldGenerator
{
    public RandomWorldGenerator(int size, int minHeight, int maxHeight) : base(size, minHeight, maxHeight){ }

    /*
     * Generates points randomly between minHeight and maxHeight
     */
    public override void generateHeightMap()
    {
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                map[x, z] = Random.Range(this.minHeight, this.maxHeight);
            }
        }
    }
}
