﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * VolcanoWorldGenerator:
 * Utilizes similar idea to Volcano Terrain Generation and Footprint Terrain Generation seen in class.
 * it generates "mountains"/"volcanos" with the given inputs at random places on the terrain.
 * If they overlap, their height adds up.
 * 
 * to generate the mountains it uses a modified -arctan() function so that the transition between base of mountain and top of mountian is gradual/smooth
 */
public class VolcanoWorldGenerator : WorldGenerator
{
    private int numVolcanos;
    private int radius;
    private int volcanoDistance;

    /*
     * VolcanoWorldGenerator Constructor
     * 
     * size: determines a the side length of the terrain
     * minHeight: determines how low the mountain starts (base of the mountain, can be negative)
     * maxHeight: determines how high the top of the mountain is
     * numVolcanos: determines how many volcanos to generate
     * radius: determines the "width" of each mountain
     * volcano distance: determines the smallest distance two volcanos can be from eachother (set value < 0 if they can be on top of eachother)
     */
    public VolcanoWorldGenerator(int size, int minHeight, int maxHeight, int numVolcanos, int radius, int volcanoDistance) : base(size, minHeight, maxHeight)
    {
        this.numVolcanos = numVolcanos;
        this.radius = radius;
        this.volcanoDistance = volcanoDistance;
    }

    /*
     * generateHeightMap() method
     * when called it generates volcanos and sets the according heights in the map array
     */
    public override void generateHeightMap()
    {
        //volcano[volcanoIndex, 0] is x coordinate
        //volcano[volcanoIndex, 1] is y coordinate
        int[,] volcanos = new int[numVolcanos, 2];

        //Generates Volcanos
        for (int i = 0; i < numVolcanos; i++)
        {
            //generate random x
            volcanos[i, 0] = Random.Range(0, size);
            //generate random y
            volcanos[i, 1] = Random.Range(0, size);

            //check if the given volcano is at the proper distance from all other volcanos
            //if not it restarts the process for this volcano until it finds a proper coordinate
            for (int j = 0; j < i; j++)
            {
                if (Mathf.Sqrt(Mathf.Pow(volcanos[i, 0] - volcanos[j, 0], 2) + Mathf.Pow(volcanos[i, 1] - volcanos[j, 1], 2)) < volcanoDistance)
                {
                    i--;
                    break;
                }
            }
        }

        //To calculate values it uses the following function (where x is the distance from the center of the volcano): 
        //f(x) = (-arctan(x * (3/radius) - 4) + PI/2.5) * (maxHeight/3f) (This is only the case while f(x) > 0)
        //To get this function I just played around in a graphing software until I got something that looked somewhat realistic

        float distanceModifier = 3f / radius;
        float yAxisConstantModifier = (Mathf.PI / 2.5f);
        float yAxisMultModifer = (maxHeight / 3f);

        //Goes through every volcano
        for (int i = 0; i < numVolcanos; i++)
        {
            //goes through every point on the map and adjusts its height accordingly
            for (int x = 0; x < size; x++)
            {
                for (int z = 0; z < size; z++)
                {
                    //Applies the function
                    float dist = (Mathf.Sqrt(Mathf.Pow(volcanos[i, 0] - x, 2) + Mathf.Pow(volcanos[i, 1] - z, 2))) * distanceModifier;
                    float height = (-Mathf.Atan(dist - 4f) + yAxisConstantModifier) * yAxisMultModifer;
                    if (height > 0)
                    {
                        map[x, z] += height;
                    }
                }
            }
        }

        //lower everything down to minHeight
        //This allows for smooth terrain when volcanos overlap
        //if the minHeight was applied each time the height of a volcano was applied there would be "harsh" transitions
        //and it would be a multiple of minHeight that would be applied
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                map[x, z] += minHeight;
            }
        }
    }
}
