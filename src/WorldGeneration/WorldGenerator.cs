﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * WorldGenerator abstract class (blueprint for different types of generators)
 */
public abstract class WorldGenerator
{
    //Used as placeholder value for coordinates that haven't yet been set
    public static readonly int NOTSET = -1;
    //Holds the height values for every coordinate
    protected float[,] map { get; private set; }
    //side length of the map
    protected int size { get; private set; }
    //min height of generator
    protected int minHeight{ get; private set; }
    //max height of generator
    protected int maxHeight { get; private set; }


    //Initializes values according to input and sets map with NOTSET value
    public WorldGenerator(int size, int minHeight, int maxHeight)
    {
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.size = size;
        this.map = new float[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                map[x, z] = WorldGenerator.NOTSET;
            }
        }
    }
    
    //When called the heights at every coordinate in map array should be set to appropriate value
    public abstract void generateHeightMap();

    //Gets height at position x, z (height is y)
    public float getHeight(int x, int z)
    {
        return map[x, z];
    }
}
